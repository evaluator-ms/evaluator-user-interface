FROM openjdk:8-jdk-alpine
LABEL maintainer="Glaud"
RUN apk --no-cache add netcat-openbsd
VOLUME /tmp
EXPOSE 9000
ARG JAR_FILE=/target/evaluator-ui-0.0.1-SNAPSHOT.war
COPY ${JAR_FILE} evaluator-ui.war
COPY ./entrypoint.sh /
RUN chmod +x /entrypoint.sh
ENTRYPOINT ["/entrypoint.sh"]