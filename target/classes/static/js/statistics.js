$("#modelSelect.stat").change(function() {
	var modelName = $("#modelSelect").val().split("/")[2];
	getModel(modelName);
	getStatistics(modelName);
});

$(".carSelect").change(function() {
	$("#statDiv").hide();
	$('#generationDiv').hide();
	$("#generationsTable > tbody").empty();

});

function getModel(modelName) {
	$.ajax({
		type : "GET",
		contentType : 'application/json',
		url : carScraperUrl + "getModel",
		data : {
			mark : markName,
			model : modelName
		},
		success : generateGenerationsTable
	});
}
function getStatistics(modelName) {
	$.ajax({
		type : "GET",
		contentType : 'application/json',
		url : priceEvaluatorUrl + "getStatistics",
		data : {
			mark : markName,
			model : modelName
		},
		success : prepareStatistics
	});
}

function generateGenerationsTable(modelDetails) {
	console.log(modelDetails);
	$.each(modelDetails.details, function(i, detail) {
		$('#generationsTable > tbody').append(
				'<tr><td>' + detail.name + '</td><td>' + detail.prodYears
						+ '</td><td>' + detail.rate + '</td></tr>');
	});
	$('#generationDiv').show();
}

function prepareCar(mark) {
	$.each(mark.models, function(i, model) {
		$('#modelSelect').append($('<option>', {
			value : model.name,
			text : model.name
		}));
	});
	$('#modelSelect').prop('disabled', false);
}

function prepareStatistics(statistics) {
	$("#statisticsCount").text(statistics.count);
	$("#statisticsAvg").text(statistics.avg);
	$("#statisticsMin").text(statistics.min);
	$("#statisticsMax").text(statistics.max);
	$("#statDiv").show();
}

function retrieveGenerations(mark, model) {
	var url = priceEvaluatorUrl + '/getGenerations?mark=' + mark + '&model='
			+ model;
	$("#generationsBlock").load(url);
}
