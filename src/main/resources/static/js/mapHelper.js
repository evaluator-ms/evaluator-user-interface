var map = null;

function initMap() {
	var directionsService = new google.maps.DirectionsService;
	var directionsDisplay = new google.maps.DirectionsRenderer;

	map = new google.maps.Map(document.getElementById('map'), {
		zoom : 11,
		center : {
			lat : 0,
			lng : 0
		},
		mapTypeId : google.maps.MapTypeId.ROADMAP
	});
	directionsDisplay.setMap(map);
	centerMap();
	calculateAndDisplayRoute(directionsService, directionsDisplay);
}

function centerMap() {
	var geocoder = new google.maps.Geocoder();
	;
	geocoder.geocode({
		'address' : origin
	}, function(results, status) {
		if (status === 'OK') {
			map.setCenter(results[0].geometry.location);
		} else {
			alert('Geocode was not successful for the following reason: '
					+ status);
		}
	});
}

function calculateAndDisplayRoute(directionsService, directionsDisplay) {
	console.log(origin + ' ' + destination);
	directionsService.route({
		origin : origin,
		destination : destination,
		travelMode : 'DRIVING'
	}, function(response, status) {
		if (status === 'OK') {
			directionsDisplay.setDirections(response);
		} else {
			window.alert('Directions request failed due to ' + status);
		}
	});
}

var placeSearch, autocomplete;

function initAutocomplete() {
	// Create the autocomplete object, restricting the search to
	// geographical
	// location types.
	autocomplete = new google.maps.places.Autocomplete(
	/** @type {!HTMLInputElement} */
	(document.getElementById('originInput')), {
		types : [ 'geocode' ]
	});
	autocomplete.addListener('place_changed', setState);

	autocomplete2 = new google.maps.places.Autocomplete(document
			.getElementById('destinationInput'), {
		types : [ 'geocode' ]
	});

}

// Bias the autocomplete object to the user's geographical location,
// as supplied by the browser's 'navigator.geolocation' object.
function geolocate() {
	if (navigator.geolocation) {
		navigator.geolocation.getCurrentPosition(function(position) {
			var geolocation = {
				lat : position.coords.latitude,
				lng : position.coords.longitude
			};
			var circle = new google.maps.Circle({
				center : geolocation,
				radius : position.coords.accuracy
			});
			autocomplete.setBounds(circle.getBounds());
		});
	}
}

function setState() {
	var place = autocomplete.getPlace();
	state = place.address_components[2].short_name;
  }
  