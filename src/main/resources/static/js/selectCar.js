$("#markSelect").change(function() {
	$("#modelSelect").find('option').not(':first').remove();
	$("#modelSelect").prop('disabled', true);
	markName = this.value;
	getCar(markName);
});

$(".carSelect").change(function() {
	isVehicleSet = false;
	$("#selectDetailsDiv").empty();
	$("#fuelDiv").empty().hide();
	$("#selectDetailsDiv").find('#engineDiv').remove();
})

function getCar(mark) {
	$.ajax({
		type : "GET",
		url : carScraperUrl + "findModelsNames",
		data : {
			"mark" : mark
		},
		success : prepareModelSelect
	});
}

function prepareModelSelect(modelsNames) {
	$.each(modelsNames, function(key, value) {
		$('#modelSelect').append($('<option>', {
			value : value,
			text : key
		}));
	});
	$('#modelSelect').prop('disabled', false);
}
$("#modelSelect").change(function() {
	getDetailsNames($(this).val());
});

function getDetailsNames(query) {
	$.ajax({
		type : "GET",
		url : carScraperUrl + "findDetailsNames",
		data : {
			"query" : query
		},
		success : prepareDetailSelect
	});
}

function prepareDetailSelect(detailsNames) {
	if (detailsNames.areEnginesReady) {
		prepareEngineSelect(detailsNames);
	} else {
		var clone = divFromGroup.clone();
		var select = $('<select class="form-control detailsSelect" />')
				.prepend('<option disabled="disabled" selected>Typ</option>');
		appendDetailsToSelect(detailsNames.names, select);
		clone.append(select);
		$("#selectDetailsDiv").append(clone);
		select.on('change', function() {
			clearInfo();
			$(this).parent().nextAll('div').remove();
			invokeGetDetailsNames($(this));
		});
	}
}

function clearInfo() {
	isVehicleSet = false;
	$("#fuelDiv").empty().hide();
	$('#engineDiv').find('span').remove();
}

function prepareEngineSelect(engines) {
	var clone = divFromGroup.clone();
	var select = $(
			'<select id="engineSelect" class="form-control detailsSelect"/>')
			.prepend('<option disabled="disabled" selected>Typ</option>');

	appendDetailsToSelect(engines.names, select);
	clone.attr('id', 'engineDiv');
	clone.append(select);
	$("#selectDetailsDiv").append(clone);
	select.on('change', function() {
		clearInfo();
		getFuelConsumtion($(this).val());
		$('#originInput').focus();
	});
}

function appendDetailsToSelect(detailsNames, select) {
	$.each(detailsNames, function(key, value) {
		select.append($('<option>', {
			value : value,
			text : key
		}));
	});
}

function getFuelConsumtion(query) {
	$.ajax({
		type : "GET",
		contentType : 'application/json',
		url : carScraperUrl + "findFuelConsumption",
		data : {
			query : query
		},
		success : showFuelConsumption
	});
}

function showFuelConsumption(fuelQuantity) {
	if (fuelQuantity.status != "OK") {
		$("#engineDiv").append(wrongSymbol.clone());
		console.log(fuelQuantity);
	} else {
		isVehicleSet = true;
		$("#engineDiv").append(okSymbol.clone());
		createCarInfo(fuelQuantity);

	}
}

function createCarInfo(fuelQuantity) {
	console.log(fuelQuantity);
	var fuelType = translateFuelType(fuelQuantity.fuelType);
	var text = 'Średnie spalanie dla wybranego auta wynosi '
			+ fuelQuantity.avgConsumption + ' litrów ' + fuelType
			+ '<br/> Spalanie deklarowane przez producenta to '
			+ fuelQuantity.avgConsumption + ' litrów ' + fuelType;
	var info = $('<span class="info"/>').append(text);
	$("#fuelDiv").append(info);
	$("#fuelDiv").show();
}

function translateFuelType(fuelType) {
	switch (fuelType) {
	case 'GASOLINE':
		return 'PB';
	case 'DIESEL':
		return 'ON';
	default:
		return fuelType;
	}
}

function invokeGetDetailsNames(e) {
	var select = $('#selectDetailsDiv');
	$('#selectDetailsDiv').off('change');
	getDetailsNames(e.val())
}