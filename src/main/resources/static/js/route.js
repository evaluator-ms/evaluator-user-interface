var okSymbol = $('<span class="checkSign"/>').append(
		'<i class="fas fa-check"></i>');
var xSymbol = $('<i class="fas fa-times"></i>');
var warnMsg = $('<span class="warnMsg">Niestety nie znaleziono raportów zużycia paliwa dla wybranego auta</span>');
var wrongSymbol = $('<span class="wrongSymbol"/>').append(xSymbol).append(
		warnMsg);
var isVehicleSet = false;

$('#originInput').keypress(function(e) {
	var key = e.which;
	if (key == 13) {
		$("#destinationInput").focus();
	}
});
$('#destinationInput').keypress(function(e) {
	var key = e.which;
	if (key == 13) {
		$('#costBtn').click();
	}
});

$(".placeDiv").change(
		function() {
			if ($(this).find('input').val().length > 2
					&& $(this).find('span').length == 0) {
				$(this).append(okSymbol.clone());
			} else {
				$(this).find('span').remove()
			}

		});

function findTravelCost() {
	if (!isVehicleSet) {
		alert("Najpierw musisz wybrać auto");
		return;
	}

	origin = $("#originInput").val();
	destination = $("#destinationInput").val();
	var carQuery = $("#engineSelect").val();
	var travelCostRequest = {
		origin : origin,
		destination : destination,
		carQuery : carQuery
	}
	getTravelCost(travelCostRequest);
	initMap();
}

function getTravelCost(travelCostRequest) {
	$.ajax({
		type : "POST",
		contentType : "application/json",
		url : routeCosterUrl + "findTravelCost",
		data : JSON.stringify(travelCostRequest),
		success : showTravelCost
	});
}

function showTravelCost(response) {
	console.log(response);
	if (response.status != "OK") {
		console.log("sadasd");
		$("#routeCostDiv").hide();
		$("#fuelFailureDiv").show();
		return;
	}
	var routeDetails = response.routeDetails;
	if (routeDetails.status != "OK") {
		$("#routeCostDiv").hide();
		$("#routeFailureDiv").show();
		return;
	}
	$("#originAddress").text(routeDetails.originAddress);
	$("#destinationAddress").text(routeDetails.destinationAddress);
	$("#routeLength").text(routeDetails.distance.value + " km");
	$("#routeCost").text(Math.round(response.cost * 100) / 100);
	$("#travelTime").text(parseDuration(routeDetails.duration));
	$("#fuelFailureDiv").hide();
	$("#routeFailureDiv").hide();
	$("#routeCostDiv").show();
}

function parseDuration(duration) {
	var text = '';
	var array = duration.match(/\d+/g);
	if (array.length == 3) {
		text += array[0] + 'h ' + array[1] + ' min i ' + array[2] + 'sek';
	} else if (array.length == 2) {
		text += array[0] + ' min i ' + array[1] + ' sek';
	} else {
		text += duration;
	}
	return text;
}

