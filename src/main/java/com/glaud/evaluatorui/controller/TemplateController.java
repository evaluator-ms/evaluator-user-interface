package com.glaud.evaluatorui.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class TemplateController {

	@GetMapping(value = { "/", "/home" })
	public String home() {
		return "home";
	}

	@GetMapping(value = "/statistics")
	public String statistics() {
		return "statistics";
	}

	@GetMapping(value = "/route")
	public String route() {
		return "route";
	}

}
