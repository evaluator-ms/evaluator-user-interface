package com.glaud.evaluatorui;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.context.annotation.Import;

import com.glaud.evaluatorui.config.AppConfig;

@SpringBootApplication
@Import(AppConfig.class)
@EnableEurekaClient
public class EvaluatorUiApplication extends SpringBootServletInitializer {

	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
		return application.sources(EvaluatorUiApplication.class);
	}

	public static void main(String[] args) {
		SpringApplication.run(EvaluatorUiApplication.class, args);
	}

}
